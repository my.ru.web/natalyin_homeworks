package ru.pcs;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.DisplayNameGeneration;
import org.junit.jupiter.api.DisplayNameGenerator;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.CsvSource;

import static org.junit.jupiter.api.Assertions.*;

@DisplayName(value = "NumberUtil is working when")
@DisplayNameGeneration(value = DisplayNameGenerator.ReplaceUnderscores.class)
class NumbersUtilTest {

    // то, что тестируем:
    private final NumbersUtil numbersUtil = new NumbersUtil();

    @Nested
    @DisplayName("gcd() is working")
    public class ForGCDTest {

        @ParameterizedTest(name = "return GCD of {0} and {1} is {2}")
        @CsvSource(value = {"18, 12, 6", "9, 12, 3", "64, 48, 16"})
        public void return_correct_GCD(int a, int b, int result) {
            assertEquals(result, numbersUtil.gcd(a, b));
        }

        @ParameterizedTest(name = "throws exception on negative {0} or {1}")
        @CsvSource(value = {"-18, 12", "9, -12", "-64, -48"})
        public void negative_numbers_throws_exception(int a, int b) {
            assertThrows(IllegalArgumentException.class, () -> numbersUtil.gcd(a, b));
        }
    }
}
