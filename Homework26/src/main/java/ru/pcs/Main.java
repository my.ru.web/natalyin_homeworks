package ru.pcs;

//import java.util.Scanner;

public class Main {
    public static void main(String[] args) {

        NumbersUtil numbersUtil = new NumbersUtil();

        /*
        НОД(18, 12) -> 6
        НОД(9, 12) -> 3
        НОД(64, 48) -> 16
        */
        System.out.println(numbersUtil.gcd(18, 12));
        System.out.println(numbersUtil.gcd(9, 12));
        System.out.println(numbersUtil.gcd(64, 48));

//        while (true) {
//            Scanner scanner = new Scanner(System.in);
//            System.out.println("\nGCS (a, b):           //exit on a = 0");
//            System.out.print(">>> enter a: ");
//            int a = Integer.parseInt(scanner.nextLine());
//            if (a == 0) return;
//            System.out.print(">>> enter b: ");
//            int b = Integer.parseInt(scanner.nextLine());
//            int res = numbersUtil.gcd(a, b);
//            int check1 = (int) (float) a / res;
//            int check2 = (int) (float) b / res;
//            System.out.printf("Result is %d (%d / %d = %d; %d / %d = %d)%n", res, a, res,check1, b, res, check2);
//        }
    }
}
