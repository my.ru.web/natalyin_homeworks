package ru.pcs;

public class NumbersUtil {
    /*
    НОД(18, 12) -> 6
    НОД(9, 12) -> 3
    НОД(64, 48) -> 16
    */
    public int gcd(int a, int b) {
        if (a < 0 || b < 0) { throw new IllegalArgumentException(); }
        if (b == 0) return a;
        return a % b == 0 ? b : gcd(b, a % b);
    }
}
