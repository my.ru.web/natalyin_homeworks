package main;

import java.util.Random;
import java.util.Arrays;
import java.lang.Integer;

public class Main {

    /**Функция нахождения числа, которое минимальное количество раз встречается в последовательности
     * при этом маркер конца последовательности -1
     * @param numbers как бы поток, последовательность
     * @param lBound нижняя граница значений в последовательности
     * @param uBound верхняя граница значений в последовательности
     * @return число с минимумом вхождений в последовательность
     */
    public static int getMinimalDuplicate(int[] numbers, int lBound, int uBound) {

        //В массиве numberCounter будем вести подсчет вхождений.
        //Каждому конкретному числу из последовательности поставим в соответствие ячейку массива.
        //Например, вхождения числа lBound будут посчитаны в numberCounter[0],
        //а вхождения числа N из интервала [uBound, lBound] будут посчитаны в numberCounter[N - lBound]
        int[] numberCounter = new int[uBound - lBound + 1];

        //Инициализируем numberCounter обнулением
        for (int i = 0; i < numberCounter.length; i++) numberCounter[i] = 0;

        int i = 0;
        while (numbers[i] != -1) {   //будем считывать числа последовательности (массива), пока не встретим -1
            numberCounter[numbers[i] - lBound]++;     //счетчик с сопоставлением числа индексу numberCounter'а
            i++;
        }

        //Далее находим, в какой ячейке находится минимальное число (вхождений),
        //причём нули будем игнорировать, т.к. это значит, что числа, в соответствие которому
        //поставлен текущий индекс, в последовательности не встретилось, и нужно чтобы значение
        //текущего значения минимального количества вхождений currentMin не менялось.
        //Также по очевидным причинам не обрабатываем -1.
        int currentMin = Integer.MAX_VALUE;     //инициализируем текущее значение заведомо максимальным
                                                //в границах условия задачи
        int currentNumber = currentMin;     //здесь будем хранить ответ; чем инициализировать - не важно
        for (int j = lBound; j < uBound + 1; j++) {
            if (numberCounter[j - lBound] != 0 && j != -1) {
                //во время работы цикла можно выводить отчет
                System.out.println("всего вхождений для числа " + j + ": " + numberCounter[j - lBound]);
                if (numberCounter[j - lBound] < currentMin) {
                    currentMin = numberCounter[j - lBound];
                    currentNumber = j;
                }
            }
        }
        return currentNumber;
    }

/*
    Homework - 07
    На вход подается последовательность чисел, оканчивающихся на -1.
    Необходимо вывести число, которе присутствует в последовательности минимальное количество раз.
    Гарантируется:
    Все числа в диапазоне от -100 до 100.
    Числа встречаются не более 2 147 483 647-раз каждое.
    Сложность алгоритма - O(n)
*/
    public static void main(String[] args) {
        //последовательность сымитируем массивом случайных чисел
        int lBound = -100;  //нижняя граница чисел последовательности
        int uBound = 100;   //верхняя граница чисел последовательности
        //длину массива, имитирующего ввод последовательности зададим случайным числом от 40 до 50
        Random random = new Random();
        int arraySize = random.nextInt(11) + 40;

        //функция создания и заполнения массива случайными числами с соблюдением условия задачи
        int[] array = getRandomArray(arraySize, lBound, uBound);
        System.out.println(Arrays.toString(array));     //распечатаем этот массив

        //Вызываем функцию для нахождения числа с мин. количеством вхождений, одновременно распечатываем результат.
        System.out.println("\nЧисло " + getMinimalDuplicate(array, lBound, uBound) +
                " присутствует в последовательности минимальное количество раз.");
    }

    //Вспомогательные подпрограммы
    /**
     * Имитирует ввод последовательности псевдослучайных целых чисел, завершающейся числом -1
     * @param arraySize длина массива (размерность)
     * @param lRndBound нижняя граница для псевдослучайных чисел
     * @param uRndBound верхняя граница для псевдослучайных чисел
     * @return возвращает массив псевдослучайных целых чисел в заданных границах с завершающим элементом -1
     */
    public static int[] getRandomArray(int arraySize, int lRndBound, int uRndBound) {
        Random random = new Random();
        int[] array = new int[arraySize];
        for (int i = 0; i < arraySize - 1; i++) {

            //у random 0 - нижня граница - включительно, верхняя - не входит, поэтому + 1
            //также выравниваем диапазон: + lRndBound
            int tmp = random.nextInt(uRndBound - lRndBound + 1) + lRndBound;

            //внутри последовательности числа -1 быть не должно
            if (tmp != -1) array[i] = tmp;
        }
        array[arraySize - 1] = -1;  //последнее число в "последовательности" должно быть -1
        return array;
    }

}
