package theme_ArrayList;

public class Main {

    /**
     * Удаление элемента по индексу
     *
     * 45, 78, 10, 17, 89, 16, size = 6
     * removeAt(3)
     * 45, 78, 10, 89, 16, size = 5
     **/
    public static void main(String[] args) {
        ArrayList<Integer> numbers = new ArrayList<>();
        numbers.add(45);    // #0
        numbers.add(78);    // #1
        numbers.add(10);    // #2
        numbers.add(17);    // #3
        numbers.add(89);    // #4
        numbers.add(16);    // #5

        System.out.print("Исходный список: ");
        System.out.println(numbers.toString());

        numbers.removeAt(3);

        System.out.print("Преобразованный список: ");
        System.out.println(numbers.toString());
    }
}
