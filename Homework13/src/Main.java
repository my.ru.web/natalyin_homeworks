import java.util.Arrays;

public class Main {
    public static void main(String[] args) {

        int[] array = {11, 12, 13, 14, 15};
        System.out.println("Исходный массив:");
        System.out.println(Arrays.toString(array));

        // Проверка на четность элементов массива.
        // Анонимная реализация метода isOk функционального интерфейса ByCondition.
        ByCondition isEven = number -> number % 2 == 0;

        System.out.println("\nЧетные элементы:");

        // Не знаю, допускается ли такая нотация, но код вполне читаем
        // и позволяет не делать бессмысленного объявления нового массива
        System.out.println(
                Arrays.toString(
                        Sequence.filter(array, isEven)      // вызов статического метода не требует
                                                            // создание экземпляра класса
                )
        );

        // Проверка на четность суммы цифр элементов массива
        // Анонимная реализация метода isOk функционального интерфейса ByCondition.
        ByCondition isDigitsSumEven = number ->  {
            int digitsSum = 0;
            while (number != 0 ) {
                int digit = number % 10;
                digitsSum += digit;
                number /= 10;
            }
            return digitsSum % 2 == 0;
        };

        System.out.println("\nЭлементы массива, сумма цифр которых четная:");

        System.out.println(
                Arrays.toString(
                        Sequence.filter(array, isDigitsSumEven)     // Вызов статического метода не требует
                                                                    // создание экземпляра класса
                )
        );
    }
}
