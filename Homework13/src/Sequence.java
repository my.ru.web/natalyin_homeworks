public class Sequence {

    public static int[] filter(int[] array, ByCondition condition) {
        int[] arrayFiltered = new int[array.length];
        int matchCount = 0;
        for (int i = 0; i < array.length; i++) {
            if (condition.isOk(array[i])) {
                arrayFiltered[matchCount] = array[i];
                matchCount++;
            }
        }

        int[] result = new int[matchCount];
        System.arraycopy(arrayFiltered, 0, result, 0, matchCount);

        return result;
    }
}
