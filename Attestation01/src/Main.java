public class Main {
    public static void main(String[] args) {
        final String fileName = "users.txt";

        // Инициализируем repositoryFile
        UsersRepositoryFile repositoryFile = new UsersRepositoryFileImpl(fileName);
        // 1|Игорь|33|true - сохраняем в репозитории такого пользователя
        // перед этим очищаем репозиторий
        repositoryFile.deleteAll();
        // Инициализируем idProvider
        IdProvider idProvider = new IdProvider(fileName);
        // после очистки репозитория idProvider возвращает id = 1 (такая логика заложена)
        User user = new User(idProvider.getId(), "Игорь", 33, true);
        repositoryFile.addUser(user);
        repositoryFile.printRepository();

        user.setName("Марсель");
        user.setAge(27);
        repositoryFile.update(user);
        repositoryFile.printRepository();
    }
}
