import java.io.*;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

public class UsersRepositoryFileImpl implements UsersRepositoryFile {
    private List<User> users = new ArrayList<>();
    private final String fileName;

    public UsersRepositoryFileImpl(String fileName) {
        this.fileName = fileName;
        try (BufferedReader reader = new BufferedReader(new FileReader(fileName))) {
            List<String> lines = reader.lines().toList();
            for (String str : lines) {
                User user = new User();
                int i = 0;
                for (String s : str.split("\\|")) {
                    i++;
                    switch (i) {
                        case 1 -> user.setId(Integer.parseInt(s));
                        case 2 -> user.setName(s);
                        case 3 -> user.setAge(Integer.parseInt(s));
                        case 4 -> user.setWorker(Boolean.parseBoolean(s));
                    }
                }
                this.users.add(user);
            }
        } catch (IOException e) {
            throw new IllegalArgumentException(e);
        }
    }

    @Override
    public List<User> getUsers() {
        return users;
    }

    @Override
    public User findById(int id) {
        if (users.stream().noneMatch(user -> user.getId() == id)) {
            throw new UnsupportedOperationException(String.format("Нет пользователей с id = %d", id));
        }
        return User.streamElementToUser(users.stream().filter(user -> user.getId() == id));
    }

    @Override
    public void update(User user) {
        Integer idToFind = user.getId();
        if (this.users.isEmpty()) {
            throw new RuntimeException(String.format("Нечего обновлять! Проверьте файл %s", fileName));
        }
        if (this.users.stream().noneMatch(u -> u.getId().equals(idToFind))) {
            throw new UnsupportedOperationException(String.format("Нет пользователей с id = %d", idToFind));
        }
        List<User> updatedUsers = this.users.stream()
                .filter(u -> !u.getId().equals(idToFind)).collect(Collectors.toList());
        updatedUsers.add(user);
        this.deleteAll();
        this.users.addAll(updatedUsers);
        this.saveAll();
    }

    @Override
    public void addUser(User user) {
        try {
            this.users.add(user);
            this.saveAll();
        } catch (RuntimeException e) {
            throw new IllegalArgumentException(e);
        }
    }

    @Override
    public void deleteUser(User user) {
        List<User> updatedUsers = this.users.stream()
                .filter(u -> u.getId() != user.getId()).collect(Collectors.toList());
        try {
            this.deleteAll();
            this.users.addAll(updatedUsers);
            this.saveAll();
        } catch (RuntimeException e) {
            throw new IllegalArgumentException(e);
        }
    }

    @Override
    public void saveAll() {
        try (BufferedWriter writer = new BufferedWriter(new FileWriter(fileName, false))) {
            for (User user : this.users) {
                writer.write(user.toRepositoryLine());
                writer.newLine();
            }
        } catch (IOException e) {
            throw new IllegalArgumentException(e);
        }
    }

    @Override
    public void deleteAll() {
        this.users = new ArrayList<>();
        try {
            this.saveAll();
        } catch (RuntimeException e) {
            throw new IllegalArgumentException(e);
        }
    }

    @Override
    public void printRepository() {
        System.out.print("[");
        System.out.println(users.getClass());
        users.forEach(User::print);
        System.out.println("]");
    }
}
