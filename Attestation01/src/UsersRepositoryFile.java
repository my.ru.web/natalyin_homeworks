import java.util.List;

public abstract interface UsersRepositoryFile {
    List<User> getUsers();
    User findById(int Id);
    void update(User user);
    void addUser(User user);
    void deleteUser(User user);
    void saveAll();
    void deleteAll();
    void printRepository();
}
