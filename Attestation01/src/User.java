import java.util.stream.Stream;

public class User {
    private Integer id;
    private String name;
    private int age;
    private boolean isWorker;

    public User() {}

    public User(Integer id, String name, int age, boolean isWorker) {
        this.id = id;
        this.name = name;
        this.age = age;
        this.isWorker = isWorker;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public boolean isWorker() {
        return isWorker;
    }

    public void setWorker(boolean worker) {
        isWorker = worker;
    }

    @Override
    public String toString() {
        return ("User {" +
                "id=" + id +
                ", name=" + name +
                ", age=" + age +
                ", isWorker=" + isWorker +
                "}");
    }

    public String toRepositoryLine() {
        return String.format("%d|%s|%d|%b", id, name, age, isWorker);
    }

    public void print() {
        System.out.println(new User(id, name, age, isWorker).toString());
    }

    public static User streamElementToUser(Stream<User> userStream) {
        return userStream.findFirst().get();
    }
}
