import java.util.ArrayList;
import java.util.List;
import java.util.TreeSet;

public class IdProvider {
    String fileName;
    TreeSet<Integer> setOfIDs = new TreeSet<>();

    public IdProvider(String fileName) {
        List<User> users;
        this.fileName = fileName;
        UsersRepositoryFile repositoryFile = new UsersRepositoryFileImpl(fileName);
        users = repositoryFile.getUsers();
        for (User u : users) {
            this.setOfIDs.add(u.getId());
        }
    }

    public Integer getId() {
        if (setOfIDs.isEmpty()) {
            this.setOfIDs.add(1);
            return 1;
        } else {
            Integer SizeOfSetOfIDs = setOfIDs.size();
            while (setOfIDs.size() == SizeOfSetOfIDs) {
                this.setOfIDs.add(setOfIDs.last() + 1);
            }
            return setOfIDs.last();
        }
    }
}
