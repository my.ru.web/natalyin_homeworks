public class BoundsAsIndexes {

    public int iLowBound;
    public int iUppBound;

    public BoundsAsIndexes(int iLowBound, int iUppBound) {
        this.iLowBound = iLowBound;
        this.iUppBound = iUppBound;
    }
}
