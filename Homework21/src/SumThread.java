public class SumThread extends Thread {

    private final int from;
    private final int to;
    private final int threadIndex;

    public SumThread(int from, int to, int threadIndex) {
        this.from = from;
        this.to = to;
        this.threadIndex = threadIndex;
    }

    @Override
    public void run() {
        for (int i = from; i < to + 1; i++) {
            Main.sums[threadIndex] += Main.array[i];
        }
    }
}
