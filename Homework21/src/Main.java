import java.util.*;

public class Main {

    public static int[] array;
    public static int[] sums;

    public static void main(String[] args) {

        System.out.print("\nЗадайте размер массива: ");

        Scanner scanner = new Scanner(System.in);
        int numbersCount = scanner.nextInt();

        System.out.print("Задайте число потоков вычисления: ");
        int threadsCount = scanner.nextInt();

        array = new int[numbersCount];
        sums = new int[threadsCount];

        Random random = new Random();

        for (int i = 0; i < numbersCount; i++) {
            array[i] = random.nextInt(101); // будут числа от 0 до 100, т.к. верхняя граница не входит
        }

//        System.out.format("Массив заполнен случайными числами от 0 до 100:\n%s\n", Arrays.toString(array));

        System.out.println("\n============================= в ы ч и с л е н и я =============================\n");

        // беспоточное суммирование
        long startTime = System.nanoTime();
        int realSum = 0;
        for (int el : array) realSum += el;
        long elapsedNanoSec = System.nanoTime() - startTime;
        double elapsedMilliSec = elapsedNanoSec / 1_000_000.0;
        System.out.format("Сумма элементов массива, посчитанная без привлечения многопоточности: %s%n", realSum);
        System.out.format("Время вычислений, мс: %f%n", elapsedMilliSec);

        // многопоточное суммирование
        startTime = System.nanoTime();

        List<BoundsAsIndexes> indexes = getDistributedBoundsAsIndexes(numbersCount, threadsCount);
        List<Thread> threadList = new ArrayList<>();

        for (int i = 0; i < threadsCount; i++) {
            Thread thread = new SumThread(indexes.get(i).iLowBound, indexes.get(i).iUppBound, i);
            threadList.add(thread);
        }

        for (Thread thread : threadList) thread.start();

        try {
            for (Thread thread : threadList) thread.join();
        } catch (InterruptedException e) {
            throw new IllegalArgumentException(e);
        }

        int byThreadSum = 0;
        for (int el : sums) byThreadSum += el;
        elapsedNanoSec = System.nanoTime() - startTime;
        elapsedMilliSec = elapsedNanoSec / 1_000_000.0;
        System.out.format("%nСумма элементов массива, многопоточное суммирование: %s%n", byThreadSum);
        System.out.format("Время вычислений " +
                "(включая время, затраченное на создание потоков), мс: %f%n", elapsedMilliSec);
    }

    public static List<BoundsAsIndexes> getDistributedBoundsAsIndexes (int numbersCount, int boundsCount) {
        List<BoundsAsIndexes> distributedBounds = new ArrayList<>();
        int multiplier = Math.round((float) numbersCount / boundsCount);
        for (int i = 0; i < boundsCount; i++) {
            int iLowBound = i * multiplier;
            int iUppBound;
            if (i == boundsCount - 1) {
                iUppBound = numbersCount - 1;
            } else {
                iUppBound = (i + 1) * multiplier - 1;
            }
            BoundsAsIndexes bounds = new BoundsAsIndexes(iLowBound, iUppBound);
            distributedBounds.add(bounds);
        }
        return distributedBounds;
    }
}
