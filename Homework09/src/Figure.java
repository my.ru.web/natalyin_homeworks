public class Figure {

    private double coordX;
    private double coordY;

    public Figure(double coordX, double coordY) {
        this.coordX = coordX;
        this.coordY = coordY;
    }

    public double getPerimeter() { return 0.0; }

    public void printPerimeter(double perimeter) {
        System.out.println("Периметр (абстрактной) фигуры: " + perimeter);
    }
}
