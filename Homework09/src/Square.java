public class Square extends Rectangle{

    public Square(double coordX, double coordY, double side) {
        super(coordX, coordY, side, side);
    }

    public void printPerimeter(double perimeter) {
        System.out.println("Периметр квадрата: " + perimeter);
    }
}
