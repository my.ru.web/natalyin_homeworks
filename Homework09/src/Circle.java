public class Circle extends Ellipse{

    public Circle(double coordX, double coordY, double radius) {
        super(coordX, coordY, radius, radius);
    }

    public double getPerimeter() {
        return 2 * Math.PI * getSemiAxis1();
    }
    public void printPerimeter(double perimeter) {
        System.out.println("Длина окружности: " +
                String.format("%.4f", perimeter).replace(",","."));
    }
}
