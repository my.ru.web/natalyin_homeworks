public class Rectangle extends Figure{

    private double sideA;     // сторона a
    private double sideB;     // сторона b

    public Rectangle(double coordX, double coordY, double sideA, double sideB) {
        super(coordX, coordY);
        this.sideA = sideA;
        this.sideB = sideB;
    }

    public double getPerimeter() {
        double perimeter = (sideA + sideB) * 2;
        return perimeter;
    }

    public void printPerimeter(double perimeter) {
        System.out.println("Периметр прямоугольника: " + perimeter);
    }

    public double getSideA() {
        return sideA;
    }

    public double getSideB() {
        return sideB;
    }
}
