public class Main {

    /*
    Homework - 09
    Сделать класс Figure, у данного класса есть два поля - x и y координаты.
    Классы Ellipse и Rectangle должны быть потомками класса Figure.
    Класс Square - потомок класса Rectangle, Circle - потомок класса
    Ellipse.
    В классе Figure предусмотреть метод getPerimeter(), который
    возвращает 0. Во всех остальных классах он должен возвращать
    корректное значение.
     */

    public static void main(String[] args) {

        Figure figure = new Figure(0,0);
        System.out.println("\nКакая-то фигура");
        figure.printPerimeter(figure.getPerimeter());

        Rectangle rectangle = new Rectangle(0, 0, 10,5);
        System.out.println("\nСтороны прямоугольника: " + rectangle.getSideA() + ", " + rectangle.getSideB());
        rectangle.printPerimeter(rectangle.getPerimeter());

        Square square = new Square(0,0,7.5);
        System.out.println("\nСторона квадрата: " + square.getSideA());
        square.printPerimeter(square.getPerimeter());

        Ellipse ellipse = new Ellipse(0,0,10,5);
        System.out.println("\nПолуоси эллипса: " + ellipse.getSemiAxis1() + ", " + ellipse.getSemiAxis2());
        ellipse.printPerimeter(ellipse.getPerimeter());

        Circle circle = new Circle(0,0, 7.5);
        System.out.println("\nРадиус окружности: " + circle.getSemiAxis1());
        circle.printPerimeter(circle.getPerimeter());
    }
}
