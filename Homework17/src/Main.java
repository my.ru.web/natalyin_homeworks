import java.util.HashMap;
import java.util.Map;
import java.util.Set;

public class Main {
    public static void main(String[] args) {

        String text = "Hashmap Hashmap Hashmap Hashmap " +              // 4 раза
                      "LinkedHashMap LinkedHashMap LinkedHashMap " +    // 3 раза
                      "Hashtable Hashtable " +                          // 2 раза
                      "TreeMap";                                        // 1 раз

        System.out.println("Используя 'Map', необходимо посчитать, " +
                "сколько встречается раз каждое слово в этой строке:");
        System.out.println(text.concat("\n"));

        String[] textSplitted = text.split(" ");

       Map<String, Integer> wordsCounter = new HashMap<>();

        for (String word : textSplitted) {
            if (wordsCounter.get(word) == null) {   //если слово встречается впервые
                wordsCounter.put(word, 1);
            } else {
                wordsCounter.put(word, wordsCounter.get(word) + 1);
            }
        }

        Set<Map.Entry<String, Integer>> entries = wordsCounter.entrySet();
        for (Map.Entry<String, Integer> entry : entries) {
            System.out.println(entry.getKey() + " - " + entry.getValue());
        }
    }
}
