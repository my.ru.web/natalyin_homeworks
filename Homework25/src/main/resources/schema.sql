create table products (
    id serial primary key,
    description varchar(100),
    price double precision,
    quantity integer check ( quantity >= 0 )
);

--truncate table products cascade;
--drop table products cascade;

create table buyers (
    id serial primary key,
    name varchar(20),
    surname varchar(20)
);

create table orders (
    id serial primary key,
    product_id integer,
    foreign key (product_id) references products(id),
    buyer_id integer,
    foreign key (buyer_id) references buyers(id),
    date date,
    quantity integer check ( quantity >= 0 )
);

-- delete (drop) table products cascade;
-- delete (drop) table orders cascade;

-- наполнение таблиц информацией
insert into products (description, price, quantity) values
    ('Алгоритмы на Java, 4-е изд. | Джитер Кевин Уэйн, Седжвик Роберт', 4191, 12);
insert into buyers (name, surname) values
    ('Сергей', 'Натальин');
insert into orders (product_id, buyer_id, date, quantity)values
    (1, 1, '2021-11-26', 1);

select id from products where id = 1;

-- обновление информации
update products set quantity = 3 where id = 1;
-- обновление информации по композиции условий
update orders set quantity = 2 where (product_id = 1 and orders.buyer_id = 1);

-- получить список товаров по возрастанию цены
select description, price from products order by price;

-- получить данные всех покупателей (name, surname) о количестве позиций в их заказах (quantity)
select name, surname, (select count(*) from orders where buyer_id = buyers.id)
    as order_items_count from buyers;

-- получить данные покупателей (name, surname), которые купили книгу о Java
select name, surname from buyers where buyers.id in
                                       (select buyer_id from orders where orders.product_id in
        (select id from products where description =
            'Алгоритмы на Java, 4-е изд. | Джитер Кевин Уэйн, Седжвик Роберт'
        )
    );

-- получить данные о покупках пользователей с вычислениями в столбце
select o.date,
       b.name,
       b.surname,
       g.description,
       g.price,
       o.quantity,
       (g.price * o.quantity) as total_cost
from buyers b
         right join orders o
                    on b.id = o.buyer_id
         left join products g
                   on g.id = o.product_id
order by o.date;

-- получить все продукты с заданной ценой
select description from products where price = 445;
