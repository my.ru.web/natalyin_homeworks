package ru.pcs;

import java.util.List;

public interface OrdersRepository {
    void save(Order order);
    List<Order> findAll();
}
