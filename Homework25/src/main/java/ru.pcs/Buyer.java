package ru.pcs;

import lombok.*;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class Buyer {
    private int id;
    private String name;
    private String surname;
}
