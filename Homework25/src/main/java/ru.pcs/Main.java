package ru.pcs;

import org.springframework.jdbc.datasource.DriverManagerDataSource;

import javax.sql.DataSource;
import java.sql.Date;

public class Main {

    public static DataSource dataSource = new DriverManagerDataSource("jdbc:postgresql://localhost:5432/schema",
            "postgres", "951");
    public static ProductsRepository productsRepository = new ProductsRepositoryJdbcTemplateImpl(dataSource);
    public static BuyersRepository buyersRepository = new BuyersRepositoryJdbcTemplateImpl(dataSource);
    public static OrdersRepository ordersRepository = new OrdersRepositoryJdbcTemplateImpl(dataSource);

    public static void main(String[] args) {
        fillTableProducts();
        fillTableBuyers();
        fillTableOrders();

        System.out.println(productsRepository.findAll());
//        System.out.println(buyersRepository.findAll());
//        System.out.println(ordersRepository.findAll());

        System.out.println(productsRepository.findAllByPrice(445));

    }

    public static void fillTableProducts() {
        productsBuilder("Песто", 445, 50);              // product_id: 1
        productsBuilder("Карбонара", 445, 50);          // product_id: 2
        productsBuilder("Пепперони", 395, 50);          // product_id: 3
        productsBuilder("Маргарита", 345, 50);          // product_id: 4
        productsBuilder("Ветчина и грибы", 345, 50);    // product_id: 5
        productsBuilder("Четыре сыра", 445, 50);        // product_id: 6
    }

    public static void fillTableBuyers() {
        buyersBuilder("Сергей", "Натальин");    // buyer_id: 1
        buyersBuilder("Василий", "Степанов");   // buyer_id: 2
        buyersBuilder("Марго", "Пиццман");      // buyer_id: 3
        buyersBuilder("Рита", "Пирожкова");     // buyer_id: 4
    }

    public static void fillTableOrders() {
        ordersBuilder(2, 1, Date.valueOf("2021-12-02"), 2);
        ordersBuilder(5, 1, Date.valueOf("2021-12-02"), 1);
        ordersBuilder(6, 1, Date.valueOf("2021-12-02"), 3);

        ordersBuilder(4, 3, Date.valueOf("2021-12-02"), 6);

        ordersBuilder(1, 4, Date.valueOf("2021-12-02"), 1);
        ordersBuilder(3, 4, Date.valueOf("2021-12-02"), 2);
        ordersBuilder(4, 4, Date.valueOf("2021-12-02"), 3);
    }

    public static void productsBuilder(String description, double price, int quantity) {
        Product product = Product.builder()
                .description(description)
                .price(price)
                .quantity(quantity)
                .build();
        productsRepository.save(product);
    }

    public static void buyersBuilder(String name, String surname) {
        Buyer buyer = Buyer.builder()
                .name(name)
                .surname(surname)
                .build();
        buyersRepository.save(buyer);
    }
    public static void ordersBuilder(int product_id, int buyer_id, Date date, int quantity) {
        Order order = Order.builder()
                .product_id(product_id)
                .buyer_id(buyer_id)
                .date(date)
                .quantity(quantity)
                .build();
        ordersRepository.save(order);
    }
}
