package ru.pcs;

import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;

import javax.sql.DataSource;
import java.sql.Date;
import java.util.List;

public class OrdersRepositoryJdbcTemplateImpl implements OrdersRepository {

    // language=SQL
    private static final String SQL_INSERT = "insert into orders(product_id, buyer_id, date, quantity) " +
            "values((select id from products where id = ?), (select id from buyers where id = ?), ?, ?)";

    // language=SQL
    private static final String SQL_SELECT_ALL = "select * from orders order by id";

    private JdbcTemplate jdbcTemplate;

    public OrdersRepositoryJdbcTemplateImpl(JdbcTemplate jdbcTemplate) {
        this.jdbcTemplate = jdbcTemplate;
    }

    public OrdersRepositoryJdbcTemplateImpl(DataSource dataSource) {
        this.jdbcTemplate = new JdbcTemplate(dataSource);
    }

    private static final RowMapper<Order> userRowMapper = (row, rowNumber) -> {
        int id = row.getInt("id");
        int product_id = row.getInt ("product_id");
        int buyer_id = row.getInt ("buyer_id");
        Date date = row.getDate ("date");
        int quantity = row.getInt ("quantity");
        return new Order(id, product_id, buyer_id, date, quantity);
    };

    @Override
    public void save(Order order) {
        jdbcTemplate.update(SQL_INSERT, order.getProduct_id(), order.getBuyer_id(), order.getDate(), order.getQuantity());
    }

    @Override
    public List<Order> findAll() {
        return jdbcTemplate.query(SQL_SELECT_ALL, userRowMapper);
    }
}
