package ru.pcs;

import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;

import javax.sql.DataSource;
import java.util.List;

public class BuyersRepositoryJdbcTemplateImpl implements BuyersRepository {

    // language=SQL
    private static final String SQL_INSERT = "insert into buyers(name, surname)" + "values(?, ?)";

    // language=SQL
    private static final String SQL_SELECT_ALL = "select * from buyers order by id";

    private JdbcTemplate jdbcTemplate;

    public BuyersRepositoryJdbcTemplateImpl(JdbcTemplate jdbcTemplate) {
        this.jdbcTemplate = jdbcTemplate;
    }

    public BuyersRepositoryJdbcTemplateImpl(DataSource dataSource) {
        this.jdbcTemplate = new JdbcTemplate(dataSource);
    }

    private static final RowMapper<Buyer> userRowMapper = (row, rowNumber) -> {
        int id = row.getInt("id");
        String name = row.getString ("name");
        String surname = row.getString ("surname");
        return new Buyer(id, name, surname);
    };

    @Override
    public List<Buyer> findAll() {
        return jdbcTemplate.query(SQL_SELECT_ALL, userRowMapper);
    }

    @Override
    public void save(Buyer buyer) {
        jdbcTemplate.update(SQL_INSERT, buyer.getName(), buyer.getSurname());
    }
}
