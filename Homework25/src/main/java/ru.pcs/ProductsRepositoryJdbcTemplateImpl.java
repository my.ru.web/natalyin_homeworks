package ru.pcs;

import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;

import javax.sql.DataSource;
import java.util.List;

public class ProductsRepositoryJdbcTemplateImpl implements ProductsRepository {

    // language=SQL
    private static final String SQL_INSERT = "insert into products(description, price, quantity)" +
            " values(?, ?, ?)";

    // language=SQL
    private static final String SQL_SELECT_ALL = "select * from products order by id";

//    // language=SQL
//    private static final String SQL_FIND_BY_PRICE = "select description from products where price = ?";

    private final JdbcTemplate jdbcTemplate;

//    public ProductsRepositoryJdbcTemplateImpl(JdbcTemplate jdbcTemplate) {
//        this.jdbcTemplate = jdbcTemplate;
//    }

    public ProductsRepositoryJdbcTemplateImpl(DataSource dataSource) {
        this.jdbcTemplate = new JdbcTemplate(dataSource);
    }

    private static final RowMapper<Product> userRowMapper = (row, rowNumber) -> {
        int id = row.getInt("id");
        String description = row.getString("description");
        double price = row.getDouble("price");
        int quantity = row.getInt("quantity");

        return new Product(id, description, price, quantity);
    };

    @Override
    public List<Product> findAll() {
        return jdbcTemplate.query(SQL_SELECT_ALL, userRowMapper);
    }

    @Override
    public List<Product> findAllByPrice(double price) {
        return findAll().stream().filter(product -> product.getPrice() == price).toList();
    }

    @Override
    public void save(Product product) {
        jdbcTemplate.update(SQL_INSERT, product.getDescription(), product.getPrice(), product.getQuantity());
    }

//    @Override
//    public List<Product> findAllByOrdersCount(int ordersCount) {
//        return null;
//    }
}
