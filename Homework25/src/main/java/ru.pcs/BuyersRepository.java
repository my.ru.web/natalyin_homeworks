package ru.pcs;

import java.util.List;

public interface BuyersRepository {
    void save(Buyer buyer);
    List<Buyer> findAll();
}
