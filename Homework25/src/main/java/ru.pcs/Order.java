package ru.pcs;

import lombok.*;

import java.sql.Date;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class Order {
    private int id;
    private int product_id;
    private int buyer_id;
    private Date date;
    private int quantity;
}
