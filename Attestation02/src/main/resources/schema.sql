create table products (
    id serial primary key,
    description varchar(100),
    price double precision check (quantity >= 0),
    quantity integer
);

insert into products (description, price, quantity) values
    ('Алгоритмы на Java, 4-е изд. | Джитер Кевин Уэйн, Седжвик Роберт', 4000, 20);
insert into products (description, price, quantity) values
    ('Java. Эффективное программирование, 3-е изд. | Джошуа Блох', 2000, 20);

select description from products where quantity = 20;

select description from products where description like '%Java%';

--truncate table products cascade;
--drop table products cascade;
--delete table products cascade;
