package ru.pcs.web.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import ru.pcs.web.models.Product;
import ru.pcs.web.repositories.ProductsRepository;

import java.math.BigDecimal;

@Controller
public class ProductsController {

    private final ProductsRepository productsRepository;

    @Autowired
    public ProductsController(ProductsRepository productsRepository) {
        this.productsRepository = productsRepository;
    }

    @PostMapping("/products")
    public String addProduct(@RequestParam("description") String description,
                             @RequestParam("price") BigDecimal price,
                             @RequestParam("quantity") Long quantity) {

//        System.out.println("description = " + description + ", " +
//                "price = " + price + ", " +
//                "quantity = " + quantity);

        Product product = Product.builder()
                .description(description)
                .price(price)
                .quantity(quantity)
                .build();

        productsRepository.save(product);

        return "redirect:/products_add.html";
    }
}
