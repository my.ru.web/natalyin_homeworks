package ru.pcs.web.repositories;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.JdbcTemplate;
//import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Component;
import ru.pcs.web.models.Product;

import javax.sql.DataSource;
//import java.math.BigDecimal;
//import java.math.BigDecimal;
import java.sql.Types;
import java.util.List;

@Component
public class ProductsRepositoryJdbcTemplateImpl implements ProductsRepository {

    private static final String SQL_INSERT = "insert into products(description, price, quantity) values(?, ?, ?)";
    private static final String SQL_SELECT_ALL = "select * from products order by description";
    private static final String SQL_FIND_BY_DESCRIPTION = "select * from products where description like ?";

    private final JdbcTemplate jdbcTemplate;

    @Autowired
    public ProductsRepositoryJdbcTemplateImpl(DataSource dataSource) {
        this.jdbcTemplate = new JdbcTemplate(dataSource);
    }

//    private static final RowMapper<Product> productRowMapper = (row, rowNumber) -> {
//        Integer id = row.getInt("id");
//        String description = row.getString("description");
//        BigDecimal price = row.getBigDecimal("price");
//        Long quantity = row.getLong("quantity");
//        return new Product(id, description, price, quantity);
//    };

    @Override
    public List<Product> findAllByDescriptionLike(String likeAnnotation) {
        return jdbcTemplate.query(SQL_FIND_BY_DESCRIPTION,
                new Object[] {likeAnnotation},
                new int[] {Types.VARCHAR},
                new BeanPropertyRowMapper<>(Product.class));
    }

    @Override
    public List<Product> findAll() {
        return jdbcTemplate.query(SQL_SELECT_ALL, new BeanPropertyRowMapper<>(Product.class));
    }

    @Override
    public void save(Product product) {
        jdbcTemplate.update(SQL_INSERT, product.getDescription(), product.getPrice(), product.getQuantity());
    }
}
