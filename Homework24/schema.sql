create table goods (
    id serial primary key,
    description varchar(100),
    cost float,
    quantity integer check ( quantity >= 0 )
);

--truncate table goods cascade;
--drop table goods cascade;

create table buyers (
    id serial primary key,
    name varchar(20),
    surname varchar(20)
);

create table orders (
    id serial primary key,
    goods_id integer,
    foreign key (goods_id) references goods(id),
    buyer_id integer,
    foreign key (buyer_id) references buyers(id),
    date date,
    quantity integer check ( quantity >= 0 )
);

insert into goods (description, cost, quantity) values
    ('Дрель-шуруповёрт Metabo PowerMaxx BS, 10.8В 34Нм, 2 АКБ', 6149, 20);
insert into goods (description, cost, quantity) values
    ('Пила дисковая Bosch PKS 55, 1.2кВт, 5600 об/мин, 90°:55мм-45°:38мм', 6442, 10);
insert into goods (description, cost, quantity) values
    ('Алгоритмы на Java, 4-е изд. | Джитер Кевин Уэйн, Седжвик Роберт', 4191, 12);
insert into goods (description, cost, quantity) values
    ('COVID-19: трудный экзамен для человечества, 2-е изд. | Супотницкий Михаил Васильевич', 1499, 1);
insert into goods (description, cost, quantity) values
    ('Проектор UNIC T5 WiFi, FullHD, BlueTooth, 16:9, 2000:1, UHE 2800лм, 3D', 7490, 2);
insert into goods (description, cost, quantity) values
    ('Живой Кофе: кофе в зернах Колумбия Богота, средняя обжарка, арабика 100%, 1кг', 907, 50);
insert into goods (description, cost, quantity) values
    ('Таблетки для посудомоечной машины Finish Powerball All-In-1-Max, 100шт.', 1099, 40);

insert into buyers (name, surname) values
    ('Сергей', 'Натальин');
insert into buyers (name, surname) values
    ('Василий', 'Степанов');
insert into buyers (name, surname) values
    ('Петр', 'Григорьев');
insert into buyers (name, surname) values
    ('Александр', 'Титов');
insert into buyers (name, surname) values
    ('Александр', 'Алексеев');

insert into orders (goods_id, buyer_id, date, quantity) values
    (1, 1, '2021-11-26', 1);
insert into orders (goods_id, buyer_id, date, quantity) values
    (2, 1, '2021-11-26', 1);
insert into orders (goods_id, buyer_id, date, quantity) values
    (3, 1, '2021-11-26', 1);
insert into orders (goods_id, buyer_id, date, quantity) values
    (4, 1, '2021-11-26', 1);
insert into orders (goods_id, buyer_id, date, quantity) values
    (6, 2, '2021-11-26', 1);
insert into orders (goods_id, buyer_id, date, quantity) values
    (7, 2, '2021-11-26', 1);
insert into orders (goods_id, buyer_id, date, quantity) values
    (6, 3, '2021-11-27', 1);
insert into orders (goods_id, buyer_id, date, quantity) values
    (5, 3, '2021-11-27', 1);
insert into orders (goods_id, buyer_id, date, quantity) values
    (5, 3, '2021-11-27', 1);
insert into orders (goods_id, buyer_id, date, quantity) values
    (5, 3, '2021-11-27', 1);
insert into orders (goods_id, buyer_id, date, quantity) values
    (5, 5, '2021-11-28', 1);
insert into orders (goods_id, buyer_id, date, quantity) values
    (6, 5, '2021-11-28', 1);

-- обновление информации
update goods set description =
    'Пила дисковая Bosch PKS 55, 1.2кВт, 5600об/мин, 90°:55мм-45°:38мм' where id = 2;

-- получить список товаров по возрастанию цены
select description, cost from goods order by cost;

-- получить данные всех покупателей (name, surname) о количестве позиций в их заказах (quantity)
select name, surname, (select count(*) from orders where buyer_id = buyers.id)
    as order_items_count from buyers;

-- получить данные покупателей (name, surname), которые купили книгу о Java
select name, surname from buyers where buyers.id in
    (select buyer_id from orders where orders.goods_id in
        (select id from goods where description =
            'Алгоритмы на Java, 4-е изд. | Джитер Кевин Уэйн, Седжвик Роберт'
        )
    );

-- получить данные о покупках пользователей
select date, name, surname, goods_id, quantity from buyers b right join orders o on b.id = o.buyer_id;
