package Solution1;

public abstract class Figure {

    //поля
    private double coordinateX;
    private double coordinateY;

    //конструктор
    public Figure(double coordinateX, double coordinateY) {
        this.coordinateX = coordinateX;
        this.coordinateY = coordinateY;
    }

    //абстрактный метод
    public abstract double getPerimeter();

    //геттер
    public double getCoordinateX() {
        return coordinateX;
    }

    //геттер
    public double getCoordinateY() {
        return coordinateY;
    }

    //сеттер
    public void setCoordinateX(double coordinateX) {
        this.coordinateX = coordinateX;
    }

    //сеттер
    public void setCoordinateY(double coordinateY) {
        this.coordinateY = coordinateY;
    }

}
