package Solution1;

public interface Movable {
    void move(double coordinateX, double coordinateY);
}
