package Solution1;

public class Main {

    /*
    Homework - 10
    Сделать класс Figure из задания 09 абстрактным.
    Определить интерфейс, который позволит перемещать фигуру на заданные координаты.
    Данный интерфейс должны реализовать только классы Circle и Square.
    В Main создать массив "перемещаемых" фигур и сдвинуть все их в одну конкретную точку.
     */

    public static void main(String[] args) {

        Circle circle = new Circle(0,0,10);
        Square square = new Square(0,0,10);

        Movable[] movables = new Movable[2];
        movables[0] = circle;
        movables[1] = square;

        double[][] originPosition = new double[2][2];
        double[][] currentPosition = new double[2][2];

        originPosition[0][0] = circle.getCoordinateX();
        originPosition[0][1] = circle.getCoordinateY();
        originPosition[1][0] = square.getCoordinateX();
        originPosition[1][1] = square.getCoordinateY();

        for (int i = 0; i < movables.length; i++) {
            if (movables[i] instanceof Movable) {
                movables[i].move(10, 10);
            }
        }

        currentPosition[0][0] = circle.getCoordinateX();
        currentPosition[0][1] = circle.getCoordinateY();
        currentPosition[1][0] = square.getCoordinateX();
        currentPosition[1][1] = square.getCoordinateY();

        printReport(movables, originPosition, currentPosition);
    }

    public static void printReport(Movable[] movables, double[][] origin, double[][] current) {
        for (int i = 0; i < movables.length; i++) {
            String displace;
            if (origin[i][0] == current[i][0] && origin[i][1] == current[i][1]) {
                displace = ": не переместилась.";
            } else {
                displace = ": переместилась.";
            }
            System.out.print("\nФигура: " + movables[i].getClass().toString() + displace);
        }
    }
}
