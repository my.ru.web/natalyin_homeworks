package Solution1;

public class Square extends Rectangle implements Movable {

    public Square(double coordinateX, double coordinateY, double side) {
        super(coordinateX, coordinateY, side, side);
    }

    public void printPerimeter(double perimeter) {
        System.out.println("Периметр квадрата: " + perimeter);
    }

    @Override
    public void move(double coordinateX, double coordinateY) {
        this.setCoordinateX(coordinateX);
        this.setCoordinateY(coordinateY);
    }
}
