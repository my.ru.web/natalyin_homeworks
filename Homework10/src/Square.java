public class Square extends Rectangle{

    public Square(double coordX, double coordY, double a) {
        super(coordX, coordY, a, a);
        this.a = a;
    }

    public void printPerimeter(double perimeter) {
        System.out.println("Периметр квадрата: " + perimeter);
    }
}
