public abstract class Figure {

    private double coordX;
    private double coordY;

    public Figure(double coordX, double coordY) {
        this.coordX = coordX;
        this.coordY = coordY;
    }

    public abstract double getPerimeter();

    public void printPerimeter(double perimeter) {
        System.out.println("Периметр (абстрактной) фигуры: " + perimeter);
    }
}
