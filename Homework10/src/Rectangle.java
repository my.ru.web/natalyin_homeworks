public class Rectangle extends Figure{

    protected double a;     // сторона a
    protected double b;     // сторона b

    public Rectangle(double coordX, double coordY, double a, double b) {
        super(coordX, coordY);
        this.a = a;
        this.b = b;
    }

    public double getPerimeter() {
        double perimeter = (a + b) * 2;
        return perimeter;
    }

    public void printPerimeter(double perimeter) {
        System.out.println("Периметр прямоугольника: " + perimeter);
    }
}
