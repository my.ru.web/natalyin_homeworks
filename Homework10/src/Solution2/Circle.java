package Solution2;

public class Circle extends Ellipse implements Movable {

public Circle(double coordinateX, double coordinateY, double radius) {
        super(coordinateX, coordinateY, radius, radius);
    }

    public double getPerimeter() {
        return 2 * Math.PI * this.getSemiAxis1();
    }

    public void printPerimeter(double perimeter) {
        System.out.println("Длина окружности: " +
                String.format("%.4f", perimeter).replace(",","."));
    }

    @Override
    public void move(double coordinateX, double coordinateY) {
        this.setCoordinateX(coordinateX);
        this.setCoordinateY(coordinateY);
    }
}
