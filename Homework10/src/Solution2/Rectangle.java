package Solution2;

public class Rectangle extends Figure{

    private double sideA;     // сторона a
    private double sideB;     // сторона b

    public Rectangle(double coordinateX, double coordinateY, double sideA, double sideB) {
        super(coordinateX, coordinateY);
        this.sideA = sideA;
        this.sideB = sideB;
    }

    public double getPerimeter() {
        return (sideA + sideB) * 2;
    }

    public void printPerimeter(double perimeter) {
        System.out.println("Периметр прямоугольника: " + perimeter);
    }

    @Override
    public void move(double coordinateX, double coordinateY) {
        return;
    }
}
