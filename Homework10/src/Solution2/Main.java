package Solution2;

public class Main {

    /*
    Homework - 10
    Сделать класс Figure из задания 09 абстрактным.
    Определить интерфейс, который позволит перемещать фигуру на заданные координаты.
    Данный интерфейс должны реализовать только классы Circle и Square.
    В Main создать массив "перемещаемых" фигур и сдвинуть все их в одну конкретную точку.
     */

    public static void main(String[] args) {

        Figure[] figures = new Figure[4];
        figures[0] = new Circle(0,0,10);
        figures[1] = new Square(0,0,10);
        figures[2] = new Ellipse(0,0,10,5);
        figures[3] = new Rectangle(0,0,10,5);

        double[][] originPosition = new double[4][2];
        double[][] currentPosition = new double[4][2];
        for (int i = 0; i < figures.length; i++) {
            originPosition[i][0] = figures[i].getCoordinateX();
            originPosition[i][1] = figures[i].getCoordinateY();
            figures[i].move(10, 10);
            currentPosition[i][0] = figures[i].getCoordinateX();
            currentPosition[i][1] = figures[i].getCoordinateY();
        }

        printReport(figures, originPosition, currentPosition);
    }

    public static void printReport(Figure[] figures, double[][] origin, double[][] current) {
        for (int i = 0; i < figures.length; i++) {
            String displace;
            if (origin[i][0] == current[i][0] && origin[i][1] == current[i][1]) {
                displace = ": не переместилась.";
            } else {
                displace = ": переместилась.";
            }
            System.out.println("Фигура: " + figures[i].getClass().toString() + displace);
        }
    }
}
