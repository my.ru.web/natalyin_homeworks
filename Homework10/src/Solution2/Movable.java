package Solution2;

public interface Movable {
    void move(double coordinateX, double coordinateY);
}
