package Solution2;

public class Ellipse extends Figure {

    private double semiAxis1;    // полуось 1
    private double semiAxis2;    // полуось 2
    
    public Ellipse(double coordinateX, double coordinateY, double semiAxis1, double semiAxis2) {
        super(coordinateX, coordinateY);
        this.semiAxis1 = semiAxis1;
        this.semiAxis2 = semiAxis2;
    }

    public double getPerimeter() {
        double perimeterMinor = Math.PI * (semiAxis1 + semiAxis2);
        double perimeterMajor = 2 * Math.PI *
                Math.sqrt((semiAxis1 * semiAxis1 + semiAxis2 * semiAxis2) / 2);
        return (perimeterMinor + perimeterMajor) / 2;
    }

    public void printPerimeter(double perimeter) {
        System.out.println("Периметр эллипса (приблизительно): " +
                String.format("%.4f", perimeter).replace(",","."));
    }

    @Override
    public void move(double coordinateX, double coordinateY) {
        return;
    }

    public double getSemiAxis1() {
        return semiAxis1;
    }
}
