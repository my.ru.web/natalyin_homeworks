public class Ellipse extends Figure {

    protected double semiAxis1;    // полуось 1
    protected double semiAxis2;      // полуось 2
    
    public Ellipse(double coordX, double coordY, double semiAxis1, double semiAxis2) {
        super(coordX, coordY);
        this.semiAxis1 = semiAxis1;
        this.semiAxis2 = semiAxis2;
    }

    public double getPerimeter() {
        double perimeterMinor = Math.PI * (semiAxis1 + semiAxis2);
        double perimeterMajor = 2 * Math.PI *
                Math.sqrt((semiAxis1 * semiAxis1 + semiAxis2 * semiAxis2) / 2);
        return (perimeterMinor + perimeterMajor) / 2;
    }

    public void printPerimeter(double perimeter) {
        System.out.println("Периметр эллипса (приблизительно): " +
                String.format("%.4f", perimeter).replace(",","."));
    }
}
