public class Circle extends Ellipse{

    protected double radius;

    public Circle(double coordX, double coordY, double radius) {
        super(coordX, coordY, radius, radius);
        this.radius = radius;
    }

    public double getPerimeter() {
        double perimeter = 2 * Math.PI * radius;
        return perimeter;
    }
    public void printPerimeter(double perimeter) {
        System.out.println("Длина окружности: " +
                String.format("%.4f", perimeter).replace(",","."));
    }
}
