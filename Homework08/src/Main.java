import java.util.Scanner;

public class Main {

    /*  Homework - 08
    На вход подается информация о людях в количестве 10 человек (имя - строка, вес - вещественное число).
    Считать эти данные в массив объектов.
    Вывести в отсортированном по возрастанию веса порядке.
     */

    public static void main(String[] args) {
        Human[] humans = new Human[10];
        Scanner sc = new Scanner(System.in);
        System.out.println("Шаблон ввода данных 'Имя; вес'");
        for (int i = 0; i < humans.length; i++) {
            humans[i] = new Human();
            System.out.print("Ввод данных, строка " + (i + 1) + ": ");
            Scanner s = new Scanner(sc.nextLine()).useDelimiter(";");
            String name = s.next();
            float weight = Float.parseFloat(s.next());
            s.close();
            humans[i].setName(name);
            humans[i].setWeight(weight);
        }

        sortHumans(humans);

        System.out.println("\nРезультат сортировки по возрастанию веса:");
        for (int i = 0; i < humans.length; i++)
            System.out.println(printHuman(humans[i]));
    }

    public static String printHuman(Human obj) {
        return obj.getName() + ": " + obj.getWeight();
    }

    public static void sortHumans(Human[] humans) {
        // Алгоритм сортировки по весу, сортировка по возрастанию
        Human tmpHuman = new Human();
        for (int i = 0; i < humans.length - 1; i++) {
            for (int j = 0; j < humans.length - 1 - i; j++) {
                if (humans[j].getWeight() > humans[j + 1].getWeight()) {
                    tmpHuman.setName(humans[j].getName());
                    tmpHuman.setWeight(humans[j].getWeight());
                    humans[j].setName(humans[j + 1].getName());
                    humans[j].setWeight(humans[j + 1].getWeight());
                    humans[j + 1].setName(tmpHuman.getName());
                    humans[j + 1].setWeight(tmpHuman.getWeight());
                }
            }
        }
    }
}
