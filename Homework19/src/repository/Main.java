package repository;

import java.util.List;
import java.util.Scanner;

/**
 * 15.11.2021
 * 20. Java IO
 *
 * @author Sidikov Marsel (First Software Engineering Platform)
 * @version v1.0
 */
public class Main {
    public static void main(String[] args) {
        UsersRepository usersRepository = new UsersRepositoryFileImpl("users.txt");
        List<User> users = usersRepository.findAll();

        for (User user : users) {
            System.out.println(user.getAge() + " " + user.getName() + " " + user.isWorker());
        }

//        User user = new User("Игорь", 33, true);
//        usersRepository.save(user);

        Scanner scanner = new Scanner(System.in);
        System.out.print("\nВведите возраст для поиска среди пользователей: ");
        int ageQuery = Integer.parseInt(scanner.nextLine());
        List<User> usersAged = usersRepository.findByAge(ageQuery);
        System.out.println("Пользователи в возрасте ".concat(Integer.toString(ageQuery)).concat(":"));
        PrintUserNamesInline(usersAged);

        List<User> usersWhoWorks = usersRepository.findByIsWorkerIsTrue();
        System.out.println("\nРаботающие пользователи:");
        PrintUserNamesInline(usersWhoWorks);
    }

    public static void PrintUserNamesInline(List<User> users) {
        if (users.size() == 0) {
            System.out.println("не найдено пользователей, соответствующих условию поиска.");
            return;
        }
        int i = 1;
        for (User user : users) {
            if  (i == 1){
                System.out.print(user.getName());
            } else {
                System.out.print(", ".concat(user.getName()));
            }
            i++;
        }
        System.out.print(".\n");
    }
}
