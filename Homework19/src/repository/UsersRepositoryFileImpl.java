package repository;

import java.io.*;
import java.util.ArrayList;
import java.util.List;

/**
 * 15.11.2021
 * 20. Java IO
 *
 * @author Sidikov Marsel (First Software Engineering Platform)
 * @version v1.0
 */
public class UsersRepositoryFileImpl implements UsersRepository {

    private String fileName;
    private List<User> users = new ArrayList<>();   // создал поле users, чтобы другие методы имели доступ
                                                    // к данным о пользователях без необходимости каждый раз
                                                    // считывать их из файла посредством метода findAll()

    /*
    Поскольку теперь методы будут знать пользователей только по полю users, необходимо обеспечить обновление
    этого поля при вызове метода save(User user)
     */
    private void addUser (User user) {
        this.users.add(user);
    }

    public UsersRepositoryFileImpl(String fileName) {
        this.fileName = fileName;
    }

    @Override
    public List<User> findAll() {
//        List<User> users = new ArrayList<>();     // убрал, т.к. создал поле private List<User> users
        // объявили переменные для доступа
        Reader reader = null;
        BufferedReader bufferedReader = null;
        try {
            // создали читалку на основе файла
            reader = new FileReader(fileName);
            // создали буферизированную читалку
            bufferedReader = new BufferedReader(reader);
            // прочитали строку
            String line = bufferedReader.readLine();
            // пока к нам не пришла "нулевая строка"
            while (line != null) {
                // разбиваем ее по |
                String[] parts = line.split("\\|");
                // берем имя
                String name = parts[0];
                // берем возраст
                int age = Integer.parseInt(parts[1]);
                // берем статус о работе
                boolean isWorker = Boolean.parseBoolean(parts[2]);
                // создаем нового человека
                User newUser = new User(name, age, isWorker);
                // добавляем его в список
                this.users.add(newUser);
                // считываем новую строку
                line = bufferedReader.readLine();
            }

        } catch (IOException e) {
            throw new IllegalArgumentException(e);
        } finally {
            // этот блок выполнится точно
            if (bufferedReader != null) {
                try {
                    // пытаемся закрыть ресурсы
                    bufferedReader.close();
                } catch (IOException ignore) {}
            }
            if (reader != null) {
                try {
                    // пытаемся закрыть ресурсы
                    reader.close();
                } catch (IOException ignore) {}
            }
        }

        return this.users;
    }

    @Override
    public void save(User user) {
        Writer writer = null;
        BufferedWriter bufferedWriter = null;
        try {
            writer = new FileWriter(fileName, true);
            bufferedWriter = new BufferedWriter(writer);

            bufferedWriter.write(user.getName() + "|" + user.getAge() + "|" + user.isWorker());
            bufferedWriter.newLine();
            addUser(user);      // обновляем поле users
//            bufferedWriter.flush();
        } catch (IOException e) {
            throw new IllegalArgumentException(e);
        } finally {
            if (bufferedWriter != null) {
                try {
                    bufferedWriter.close();
                } catch (IOException ignore) {}
            }
            if (writer != null) {
                try {
                    writer.close();
                } catch (IOException ignore) {}
            }
        }
    }

    @Override
    public List<User> findByAge(int age) {
        // TODO: реализовать
//        List<User> users = new UsersRepositoryFileImpl(fileName).findAll();
        List<User> usersByAge = new ArrayList<>();
        for (User user : users) {
            if (user.getAge() == age) {
                usersByAge.add(user);
            }
        }
        return usersByAge;
    }

    @Override
    public List<User> findByIsWorkerIsTrue() {
        // TODO: реализовать
//        List<User> users = new UsersRepositoryFileImpl(fileName).findAll();
        List<User> usersWhoWorks = new ArrayList<>();
        for (User user : users) {
            if (user.isWorker()) {
                usersWhoWorks.add(user);
            }
        }
        return usersWhoWorks;
    }
}
