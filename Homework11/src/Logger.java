public class Logger {

    private static final Logger instance;

    static {
        instance = new Logger();
    }

    private Logger() {
        this.message = "";
        this.count = 0;
    }

    public static Logger getInstance() {
        return instance;
    }

    private static final int MAX_MESSAGE_COUNT = 10;

    private String message;
    private int count;

    public void add(String message) {
        if (count < MAX_MESSAGE_COUNT) {
            this.message += message + "\n";
            count++;
        } else {
            System.err.println("Переполнение логгера.");
        }
    }

    void log(String message) {
        System.out.println(message);
    }

    public String getMessage() {
        return message;
    }
}
