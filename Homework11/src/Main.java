public class Main {
    public static void main(String[] args) {
        String[] record = new String[12];
        for (int i = 0; i < record.length; i++) {
            record[i] = "Запись #" + (i + 1);
            Logger.getInstance().add(record[i]);
            Logger.getInstance().log(record[i]);
        }

        System.out.println("\nРаспечатка лога: ");
        Logger.getInstance().log(Logger.getInstance().getMessage());
    }
}
