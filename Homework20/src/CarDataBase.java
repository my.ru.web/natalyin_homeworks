import java.util.List;

public interface CarDataBase {
    List<String> readDB();
    List<Car> parseByCars(String delimiter);
    List<Car> findByBrand(String brand);
    List<Car> filterByRegEx(String regExpression);
    List<Car> filterByCondition(boolean IsSuitable);
    void printCarDataBase();
}
