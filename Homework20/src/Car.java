import java.util.Objects;

public class Car {
    private String stateNumber;
    private String brand;
    private String color;
    private int kilometerage;
    private int price;

    public Car() {
    }

    public Car(String stateNumber, String brand, String color, int kilometerage, int price) {
        this.stateNumber = stateNumber;
        this.brand = brand;
        this.color = color;
        this.kilometerage = kilometerage;
        this.price = price;
    }

    public String getStateNumber() {
        return stateNumber;
    }

    public String getBrand() {
        return brand;
    }

    public String getColor() {
        return color;
    }

    public int getKilometerage() {
        return kilometerage;
    }

    public int getPrice() {
        return price;
    }

    public void setStateNumber(String stateNumber) {
        this.stateNumber = stateNumber;
    }

    public void setBrand(String brand) {
        this.brand = brand;
    }

    public void setColor(String color) {
        this.color = color;
    }

    public void setKilometerage(int kilometerage) {
        this.kilometerage = kilometerage;
    }

    public void setPrice(int price) {
        this.price = price;
    }

    public void toPrint() {
        System.out.println("Car {" +
                "stateNumber=" + stateNumber +
                ", brand=" + brand +
                ", color=" + color +
                ", kmAge=" + kilometerage +
                ", price=" + price +
                "}");
    }
}
