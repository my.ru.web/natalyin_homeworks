import java.io.*;
import java.util.*;

import static java.lang.Integer.parseInt;

public class CarDataBaseImpl implements CarDataBase {

    private final String fileName;
    private List<String> carsAsListOfString = new ArrayList<>();
    private List<Car> carsList = new ArrayList<>();

    public CarDataBaseImpl(String fileName) {
        this.fileName = fileName;
    }

    @Override
    public List<String> readDB() {
        try (BufferedReader reader = new BufferedReader(new FileReader(fileName))) {
            // В этом примере дубли машин не имеют смысла и даже вредны, например при расчете средней цены.
            // Избавимся от одинаковых строк, но при этом "базу данных" менять не будем,
            // т.к. задачи "почистить" данные в файле у нас не было.
            this.carsAsListOfString = reader.lines().distinct().toList();
//            System.out.println("<---begin");
//            carsAsListOfString.forEach(System.out::println);
//            System.out.println("end--->");
        } catch (IOException e) {
            throw new IllegalArgumentException(e);
        }
        return carsAsListOfString;
    }

    @Override
    public List<Car> parseByCars(String delimiter) {
        for (String str : carsAsListOfString) {
            Car car = new Car();
            int i = 0;
            for (String s : str.split(delimiter)) {
                i++;
                switch (i) {
                    case 1 -> car.setStateNumber(s);
                    case 2 -> car.setBrand(s);
                    case 3 -> car.setColor(s);
                    case 4 -> car.setKilometerage(parseInt(s));
                    case 5 -> car.setPrice(parseInt(s));
                }
            }
            carsList.add(car);
        }
        return carsList;
    }

    @Override
    public List<Car> findByBrand(String brand) {
        return null;
    }

    @Override
    public List<Car> filterByRegEx(String regExpression) {
        return null;
    }

    @Override
    public List<Car> filterByCondition(boolean IsSuitable) {
        return null;
    }

    @Override
    public void printCarDataBase() {
        System.out.print("[");
        System.out.println(carsList.getClass());
        carsList.forEach(Car::toPrint);
        System.out.println("]");
    }
}
