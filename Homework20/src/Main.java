import java.util.*;
import java.util.stream.Collectors;
/*
    Используя Java Stream API, вывести:
    - номера всех автомобилей, имеющих черный цвет или нулевой пробег. // filter + map
    - количество уникальных моделей в ценовом диапазоне от 700 до 800 тыс. // distinct + filter
    * - цвет автомобиля с минимальной стоимостью. // min + map
    * - среднюю стоимость Camry
    https://habr.com/ru/company/luxoft/blog/270383/
 */

public class Main {
    public static void main(String[] args) {

        CarDataBase carDataBase = new CarDataBaseImpl("data.txt");

        List<String> carsAsListOfString = carDataBase.readDB();

        List<Car> carsList = carDataBase.parseByCars("\\|");
        System.out.println("Разобранные (уникальные) данные исходного файла:");
        carDataBase.printCarDataBase();

        // Решение первой задачи
        System.out.println("\nЧерные, либо с нулевым пробегом:");
        carsList.stream()
                .filter(car -> car.getColor().equals("Black") || car.getKilometerage() == 0)
                .collect(Collectors.toList())
                .forEach(Car::toPrint);

        // Решение второй задачи
        System.out.println("\nКоличество уникальных моделей в ценовом диапазоне от 700 до 800 тыс.:");
        List<Car> tmpCarsList = carsList.stream()
                .filter(car -> car.getPrice() >= 700_000 && car.getPrice() <= 800_000).collect(Collectors.toList());
        System.out.printf("найдено %d:%n", tmpCarsList.size());
        tmpCarsList.forEach(Car::toPrint);

        // Решение третьей задачи
        System.out.println("\nЦвет автомобиля с минимальной стоимостью:");
        Map<String, Integer> colorToPrice = new HashMap<>();
        carsList.stream()
                .forEach(car -> {
                    String color = car.getColor();
                    Integer price = car.getPrice();
                    if (colorToPrice.get(color) == null || colorToPrice.get(color) > price) {
                    colorToPrice.put(color, price);
                }
                });
        int minPrice = Integer.MAX_VALUE;
        String colorForMinPrice = "";
        Set<Map.Entry<String, Integer>> entries = colorToPrice.entrySet();
        for (Map.Entry<String, Integer> entry : entries) {
            if (minPrice > entry.getValue()) {
                minPrice = entry.getValue();
                colorForMinPrice = entry.getKey();
            }
        }
        System.out.println(colorForMinPrice);

        // Решение четвертой задачи
        System.out.println("\nСредняя стоимость Camry:");
        tmpCarsList = carsList.stream()
                .filter(car -> car.getBrand().equals("Camry")).collect(Collectors.toList());
        int sumOfCamryPrices = tmpCarsList.stream().mapToInt(car -> car.getPrice()).sum();
        int numberOfCamryOffers = tmpCarsList.size();
        System.out.println(sumOfCamryPrices / numberOfCamryOffers);
    }
}
